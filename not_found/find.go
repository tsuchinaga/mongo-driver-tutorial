package main

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

type Object struct {
	ID string `bson:"_id"`
}

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags | log.Lmicroseconds)

	user := "mongo"
	password := "password"
	host := "mongo-driver-db"

	// コネクションを張る
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s", user, password, host))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatalln(err)
	}
	defer client.Disconnect(context.TODO())

	coll := client.Database("tags").Collection("objects")
	err = coll.Drop(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}

	insertResult, err := coll.InsertOne(context.TODO(), Object{ID: primitive.NewObjectID().Hex()})
	if err != nil {
		log.Fatalln(err)
	}
	id := insertResult.InsertedID

	var result Object

	// 見つかる場合
	log.Println("・見つかる場合")
	findResult := coll.FindOne(context.TODO(), bson.M{"_id": id})
	if findResult.Err() != nil {
		log.Fatalln(findResult.Err())
	}
	if err := findResult.Decode(&result); err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", result)

	// 見つからない場合
	log.Println("・見つからない場合")
	findResult = coll.FindOne(context.TODO(), bson.M{"_id": ""})
	if findResult.Err() != nil {
		log.Println("ErrNoDocuments", errors.Is(findResult.Err(), mongo.ErrNoDocuments))
		log.Println(findResult.Err()) // -> mongo: no documents in result
	}
	if err := findResult.Decode(&result); err != nil {
		log.Println("ErrNoDocuments", errors.Is(findResult.Err(), mongo.ErrNoDocuments))
		log.Println(err)
	}
}
