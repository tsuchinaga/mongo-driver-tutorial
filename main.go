package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

type Trainer struct {
	ID        string `bson:"_id"`
	Name      string
	Age       int
	City      string
	Alive     bool
	CreatedAt time.Time
}

func main() {
	fmt.Println("こんにちわーるど")

	log.SetFlags(log.Lshortfile | log.LstdFlags | log.Lmicroseconds)

	mongodb()
}

func mongodb() {
	user := "mongo"
	password := "password"
	host := "mongo-driver-db"

	// コネクションを張る
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s", user, password, host))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatalln(err)
	}

	// 接続確認
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Connected to MongoDB!")

	// 最後には切断処理をするようにする
	defer func() {
		err = client.Disconnect(context.TODO())
		if err != nil {
			log.Fatalln(err)
		}
		log.Println("Connection to MongoDB closed.")
	}()

	// データベースの指定
	database := client.Database("tutorial")
	err = database.Drop(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}

	// コレクションの指定
	collection := database.Collection("trainers")
	err = collection.Drop(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}

	ashID := primitive.NewObjectID().Hex()
	mistyID := primitive.NewObjectID().Hex()
	brockID := primitive.NewObjectID().Hex()
	log.Println("ids: ", ashID, mistyID, brockID)
	ashObjectId, _ := primitive.ObjectIDFromHex(ashID)
	mistyObjectId, _ := primitive.ObjectIDFromHex(mistyID)
	brockObjectId, _ := primitive.ObjectIDFromHex(brockID)
	log.Println("objectIds: ", ashObjectId, mistyObjectId, brockObjectId)
	ash := Trainer{ID: ashID, Name: "Ash", Age: 10, City: "Pallet Town", Alive: true, CreatedAt: time.Date(2009, 12, 6, 0, 0, 0, 0, time.Local)}
	misty := Trainer{ID: mistyID, Name: "Misty", Age: 10, City: "Cerulean City", Alive: true, CreatedAt: time.Date(2000, 4, 1, 0, 0, 0, 0, time.Local)}
	brock := Trainer{ID: brockID, Name: "Brock", Age: 15, City: "Pewter City", Alive: false, CreatedAt: time.Date(2010, 3, 26, 12, 30, 30, 5, time.Local)}
	log.Println("trainers: ", ash, misty, brock)
	// 1つのドキュメントをinsert
	insertResult, err := collection.InsertOne(context.TODO(), ash)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Inserted a single document: ", insertResult)
	// mongodbでinsertされていることを確認
	// > use tutorial
	// switched to db tutorial
	// > db.trainers.find()
	// { "_id" : ObjectId("5de7b710d3edba62c0877fef"), "name" : "Ash", "age" : 10, "city" : "Pallet Town" }

	// 複数ドキュメントまとめてinsert
	insertManyResult, err := collection.InsertMany(context.TODO(), []interface{}{misty, brock})
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("Inserted a multiple documents: ", insertManyResult.InsertedIDs)
	// mongodbでinsertされていることを確認
	// > use tutorial
	// switched to db tutorial
	// > db.trainers.find()
	// { "_id" : ObjectId("5de7bb9810b419501743bade"), "name" : "Ash", "age" : 10, "city" : "Pallet Town" }
	// { "_id" : ObjectId("5de7bb9810b419501743badf"), "name" : "Misty", "age" : 10, "city" : "Cerulean City" }
	// { "_id" : ObjectId("5de7bb9810b419501743bae0"), "name" : "Brock", "age" : 15, "city" : "Pewter City" }

	// 1つのドキュメントをupdate
	filter := bson.D{{"name", "Ash"}}
	update := bson.D{{"$inc", bson.D{{"age", 1}}}}
	updateResult, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("Matched %d documents and updated %d documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
	// mongoでインクリメントされていることを確認
	// > use tutorial
	// switched to db tutorial
	// > db.trainers.find()
	// { "_id" : ObjectId("5de829120a895acaaa626195"), "name" : "Ash", "age" : 11, "city" : "Pallet Town" }
	// { "_id" : ObjectId("5de829120a895acaaa626196"), "name" : "Misty", "age" : 10, "city" : "Cerulean City" }
	// { "_id" : ObjectId("5de829120a895acaaa626197"), "name" : "Brock", "age" : 15, "city" : "Pewter City" }

	// 1つのドキュメントをfind
	var result Trainer
	err = collection.FindOne(context.TODO(), bson.D{{"name", "Ash"}}).Decode(&result)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("Found a single document: %+v\n", result)

	// 複数のドキュメントをfind
	var results []Trainer
	cur, err := collection.Find(context.TODO(), bson.D{{}}, options.Find().SetLimit(2))
	if err != nil {
		log.Fatalln(err)
	}

	for cur.Next(context.TODO()) {
		var result Trainer
		err := cur.Decode(&result)
		if err != nil {
			log.Fatalln(err)
		}
		results = append(results, result)
	}
	if cur.Err() != nil {
		log.Fatalln(cur.Err())
	}

	_ = cur.Close(context.TODO())
	log.Printf("Found multiple documents: %+v\n", results)

	// time.Timeを条件に取得する
	cur, err = collection.Find(context.TODO(), bson.D{{"createdat", bson.D{{"$gte", ash.CreatedAt}}}})
	if err != nil {
		log.Fatalln(err)
	}

	for cur.Next(context.TODO()) {
		var result Trainer
		err := cur.Decode(&result)
		if err != nil {
			log.Fatalln(err)
		}
		result.CreatedAt = result.CreatedAt.In(time.Local)
		log.Println(result)
	}
	_ = cur.Close(context.TODO())

	taro := Trainer{ID: primitive.NewObjectID().Hex(), Name: "Taro", Age: 20, City: "Tokyo", Alive: true, CreatedAt: time.Date(2000, 4, 1, 0, 0, 0, 0, time.Local)}
	hana := Trainer{ID: primitive.NewObjectID().Hex(), Name: "Hana", Age: 18, City: "Osaka", Alive: true, CreatedAt: time.Date(2002, 4, 1, 0, 0, 0, 0, time.Local)}
	tsu := Trainer{ID: primitive.NewObjectID().Hex(), Name: "Tsu", Age: 20, City: "Osaka", Alive: true, CreatedAt: time.Date(2000, 4, 1, 0, 0, 0, 0, time.Local)}
	_, err = collection.InsertMany(context.TODO(), []interface{}{taro, hana, tsu})
	if err != nil {
		log.Fatalln(err)
	}

	// AND条件
	log.Println("・AND条件")
	cur, err = collection.Find(context.TODO(), bson.D{{"age", 18}, {"city", "Osaka"}})
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// AND条件(マップ)
	log.Println("・AND条件(マップ)")
	cur, err = collection.Find(context.TODO(), bson.M{"age": 18, "city": "Osaka"})
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// OR条件
	log.Println("・OR条件")
	cur, err = collection.Find(context.TODO(), bson.M{"$or": []bson.M{{"age": 18}, {"city": "Tokyo"}}})
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// 比較条件
	log.Println("・比較条件")
	cur, err = collection.Find(context.TODO(), bson.M{"age": bson.M{"$gte": 20}})
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// ソート
	log.Println("・ソート")
	cur, err = collection.Find(context.TODO(), bson.M{}, options.Find().SetSort(bson.M{"age": -1}))
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// リミット・オフセット1
	log.Println("・リミット・オフセット1")
	cur, err = collection.Find(context.TODO(), bson.M{}, options.Find().SetSort(bson.M{"age": -1}).SetLimit(3).SetSkip(2))
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// リミット・オフセット2
	log.Println("・リミット・オフセット2")
	cur, err = collection.Find(context.TODO(), bson.M{}, options.Find().SetSkip(2).SetLimit(3).SetSort(bson.M{"age": -1}))
	if err != nil {
		log.Fatalln(err)
	}
	PrintCursor(cur)
	_ = cur.Close(context.TODO())

	// 集計
	log.Println("・カウント")
	cur, err = collection.Aggregate(context.TODO(), bson.A{bson.M{"$group": bson.M{
		"_id":   bson.M{"age": "$age"},
		"count": bson.M{"$sum": 1},
	}}})
	if err != nil {
		log.Fatalln(err)
	}
	for cur.Next(context.TODO()) {
		var result struct {
			Key   map[string]int `bson:"_id"`
			Count int
		}
		if err := cur.Decode(&result); err != nil {
			log.Fatalln(err)
		}
		log.Printf("%+v\n", result)
	}
	_ = cur.Close(context.TODO())

	// まとめて取り出し
	cur, err = collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatalln(err)
	}
	if err = cur.All(context.TODO(), &results); err != nil {
		log.Fatalln(err)
	}
	_ = cur.Close(context.TODO())
	log.Printf("%+v\n", results)

	// delete
	deleteResult, err := collection.DeleteMany(context.TODO(), bson.D{{}})
	if err != nil {
		log.Fatalln(cur.Err())
	}
	log.Printf("Deleted %v documents in the trainers collection\n", deleteResult.DeletedCount)
}

func PrintCursor(cursor *mongo.Cursor) {
	for cursor.Next(context.TODO()) {
		var result Trainer
		if err := cursor.Decode(&result); err != nil {
			log.Fatalln(err)
		}
		log.Printf("%+v\n", result)
	}
}
