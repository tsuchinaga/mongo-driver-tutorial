package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"math"
	"time"
)

type User struct {
	ID        string             `bson:"_id"`
	Name      string             `bson:"name"`
	Age       int                `bson:"age"`
	Tags      []string           `bson:"tags"`
	Scores    map[string]float64 `bson:"scores"`
	CreatedAt time.Time          `bson:"createdAt"`
}

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags | log.Lmicroseconds)

	user := "mongo"
	password := "password"
	host := "mongo-driver-db"

	// コネクションを張る
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s", user, password, host))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatalln(err)
	}
	defer client.Disconnect(context.TODO())

	// 全項目有効数
	user1 := User{
		ID:        primitive.NewObjectID().Hex(),
		Name:      "user 1",
		Age:       math.MaxInt64,
		Tags:      []string{"tag1", "tag2", "tag3"},
		Scores:    map[string]float64{"2019/12/07": math.MaxFloat64, "2019/12/08": math.SmallestNonzeroFloat64},
		CreatedAt: time.Now(),
	}

	// 手動でゼロ値
	user2 := User{
		ID:        primitive.NewObjectID().Hex(),
		Name:      "",
		Age:       0,
		Tags:      []string{},
		Scores:    map[string]float64{},
		CreatedAt: time.Time{},
	}

	// 自動でゼロ値
	user3 := User{
		ID: primitive.NewObjectID().Hex(),
	}

	coll := client.Database("tags").Collection("users")
	err = coll.Drop(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}

	_, err = coll.InsertMany(context.TODO(), []interface{}{user1, user2, user3})
	if err != nil {
		log.Fatalln(err)
	}

	cursor, err := coll.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatalln(err)
	}

	for cursor.Next(context.TODO()) {
		var result map[string]interface{}
		if err := cursor.Decode(&result); err != nil {
			log.Fatalln(err)
		}
		log.Printf("%+v\n", result)
	}
}
