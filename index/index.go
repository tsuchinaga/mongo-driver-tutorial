package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags | log.Lmicroseconds)

	user := "mongo"
	password := "password"
	host := "mongo-driver-db"

	// コネクションを張る
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s", user, password, host))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatalln(err)
	}
	defer client.Disconnect(context.TODO())

	// コレクションの指定
	coll := client.Database("index").Collection("index")
	err = coll.Drop(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}

	// インデックス操作
	cursor, err := coll.Indexes().List(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}
	var indexList []bson.M
	if err = cursor.All(context.TODO(), &indexList); err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", indexList)

	indexName, err := coll.Indexes().CreateOne(context.TODO(), mongo.IndexModel{Keys: bson.M{"age": 1}})
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(indexName)

	cursor, err = coll.Indexes().List(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}
	if err = cursor.All(context.TODO(), &indexList); err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", indexList)

	indexName, err = coll.Indexes().CreateOne(context.TODO(), mongo.IndexModel{Keys: bson.M{"age": 1}})
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(indexName)

	cursor, err = coll.Indexes().List(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}
	if err = cursor.All(context.TODO(), &indexList); err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", indexList)

	// ユニークキー制約
	log.Println("・ユニークキー制約")
	indexName, err = coll.Indexes().CreateOne(context.TODO(), mongo.IndexModel{Keys: bson.M{"name": 1}, Options: options.Index().SetUnique(true)})
	if err != nil {
		log.Fatalln(err)
	}
	log.Println(indexName)

	cursor, err = coll.Indexes().List(context.TODO())
	if err != nil {
		log.Fatalln(err)
	}
	if err = cursor.All(context.TODO(), &indexList); err != nil {
		log.Fatalln(err)
	}
	log.Printf("%+v\n", indexList)
}
